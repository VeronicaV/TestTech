﻿using System;
using TechTestGUI;
using TechTestData;
using TechRover;


using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TechTestRover
{
    [TestClass]
    public class TestRover
    {
        [TestMethod]  
        public void roverposition()
        { 
            RoverBussines r = new RoverBussines();
     //     Assert.AreEqual("0,0,0", r.position("L")); example incorrect
            Assert.AreEqual("0,0,West", r.processPosition("L"));
        }

        [TestMethod]
        public void initialposition()
        {
            RoverBussines p = new RoverBussines();
            p.PositionInitial();
        }

        [TestMethod]
        public void processLaunch()
        {
            LauncherRover l = new LauncherRover();
            //     Assert.AreEqual("0,0,0", l.processRover("L")); example incorrect
            Assert.AreEqual("0,0,West", l.processRover("L"));
        }

        [TestMethod]
        public void validcommadLaunch()
        {
            LauncherRover l = new LauncherRover();
            Assert.AreEqual(true, l.validCommand("P"));
           // Assert.AreEqual(false, l.validCommand("P"));
        }

        [TestMethod]
        public void uppercommandLaunch()
        {
            LauncherRover l = new LauncherRover();
            //  Assert.AreEqual(false, l.validCommand("l"));        Incorrect       
            // Assert.AreEqual(true, l.validCommand("L"));          Incorrect
            //Assert.AreEqual(true, l.validCommand("l"));           Correct
            Assert.AreEqual(false, l.validCommand("L"));         // Correct
        }

        [TestMethod]
        public void validgridPosition()
        {
            RoverBussines r = new RoverBussines();
           // Assert.AreEqual(false, r.gridPosition("0,0,3")); //incorrect, wait true
          //  Assert.AreEqual(false, r.gridPosition("7,0,3")); // correct wait false
            Assert.AreEqual(true, r.gridPosition("0,0,3"));
        }
    }
}
