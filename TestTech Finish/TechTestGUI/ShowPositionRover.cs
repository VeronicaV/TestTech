﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestGUI
{
    class ShowPositionRover
    {
        public void showInitial(string result)
        {
            String[] writeShow = result.Split(',');
            string roverPositionX = writeShow[0];
            string roverPositionY = writeShow[1];
            string roverFacing = writeShow[2];
            Console.WriteLine($"Rover Position initial {roverPositionX}, {roverPositionY} - facing {roverFacing}");
        }

        public void showRover(string result)
        {   
            String[] writeShow = result.Split(',');
            if (writeShow.Count() == 1)
            {
                Console.WriteLine(result);
            }
            else
            {
                string roverPositionX = writeShow[0];
                string roverPositionY = writeShow[1];
                string roverFacing = writeShow[2];
                Console.WriteLine($"Rover is now at ({roverPositionX}, {roverPositionY}) - facing {roverFacing}");
            }
        }

        public void messageError(string result)
        {
            String[] writeShow = result.Split(',');
            string roverPositionX = writeShow[0];
            string roverPositionY = writeShow[1];
            string roverFacing = writeShow[2];
            Console.WriteLine($"Incorrect position, out of grid ({roverPositionX}, {roverPositionY}) - facing {roverFacing}");
        }

        public void message(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
