﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestData
{
    public class Position
    {
        public int roverPositionX;
        public int roverPositionY;
        public RoverFacing roverFacing;
        public enum RoverFacing
        {
            North,
            East,
            South,
            West
        }

        public Position() { }

        public RoverFacing facing(string rover)
        {
            if (rover == "North")
                return RoverFacing.North;
            if (rover == "East")
                return RoverFacing.East;
            if (rover == "South")
                return RoverFacing.South;
            if (rover == "West")
                return RoverFacing.West;
            else
                return RoverFacing.North;        
        }
    }
}
