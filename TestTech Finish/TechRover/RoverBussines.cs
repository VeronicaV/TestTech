﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestData;

namespace TechRover
{
    public class RoverBussines
    {
        private Position r = new Position();
        public RoverBussines() { }
      
        public string processPosition(string command)
        {

            switch (command)
            {
                case "L":
                    r.roverFacing = r.roverFacing == Position.RoverFacing.North ? Position.RoverFacing.West : (Position.RoverFacing)((int)r.roverFacing - 1);                   
                    break;
                case "R":
                    r.roverFacing = r.roverFacing == Position.RoverFacing.West ? Position.RoverFacing.North : (Position.RoverFacing)((int)r.roverFacing - 1);
                    break;
                case "F":
                    switch (r.roverFacing)
                    {
                        case Position.RoverFacing.North:
                            r.roverPositionX++;
                            break;
                        case Position.RoverFacing.East:
                            r.roverPositionY++;
                            break;
                        case Position.RoverFacing.South:
                            r.roverPositionX--;
                            break;
                        case Position.RoverFacing.West:
                            r.roverPositionY--;
                            break;
                    }                    
                    break;
                default:
                    return ("invalid command");                    
            }
            return String.Format($"{r.roverPositionX},{r.roverPositionY},{r.roverFacing}");
        }
        public string PositionInitial()
        {
            r.roverPositionX = 0;
            r.roverPositionY = 0;
            r.roverFacing = Position.RoverFacing.North;
            return String.Format($"{r.roverPositionX},{r.roverPositionY},{r.roverFacing}");

        }

        public Boolean gridPosition(string position)
        {
            String[] rover = position.Split(',');
            string roverPositionX = rover[0];
            string roverPositionY = rover[1];
            string roverFacing = rover[2];
            int positionX = int.Parse(roverPositionX);
            int positionY = int.Parse(roverPositionY);

            if (0 <= positionX && positionX <= 5 && 0 <= positionY && positionY <= 5)
                return true;
            else
                return false;
        }

        public void changePosition(string position)
        {
            String[] rover = position.Split(',');
            int roverPositionX = int.Parse(rover[0]);
            int roverPositionY = int.Parse(rover[1]);
            string roverFacing = rover[2];
            r.roverPositionX = roverPositionX;
            r.roverPositionY = roverPositionY;
            r.roverFacing = r.facing(roverFacing);
        }

       
    }
}
